﻿using Spaceship;
using UnityEngine;

namespace EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;

        private GameObject playerSpaceship;
        
        private void Awake()
        {
            playerSpaceship = GameObject.FindWithTag("Player");
            
            InvokeRepeating("OnFire", 1, 1.5f);
        }

        private void Update()
        {
            MoveToPlayer();
        }
        
        private void OnFire()
        {
            enemySpaceship.Fire();
        }

        private void MoveToPlayer()
        {
            var directionToPlayer = playerSpaceship.transform.position - transform.position;
            var velocity = directionToPlayer.normalized * enemySpaceship.Speed;
            
            transform.Translate(new Vector3(velocity.x,0,0) * Time.deltaTime);
        }
    }    
}

