using System;
using DefaultNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Manager
{
    public class UIManager: Singleton<UIManager>
    {
        [SerializeField] private Button startButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button quitButton;
        [SerializeField] private Button nextLevelButton;
        [SerializeField] private TextMeshProUGUI text;
        [SerializeField] private RectTransform dialog;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI laserText;
        [SerializeField] private TextMeshProUGUI levelText;
        
        private void Awake()
        {
            Debug.Assert(startButton != null, "startButton cannot be null");
            Debug.Assert(restartButton != null, "restartButton cannot be null");
            Debug.Assert(quitButton != null, "quitButton cannot be null");
            Debug.Assert(nextLevelButton != null, "nextLevelButton cannot be null");
            Debug.Assert(text != null, "text cannot be null"); 
            Debug.Assert(dialog != null, "dialog cannot be null");
            Debug.Assert(scoreText != null, "scoreText cannot be null");
            Debug.Assert(laserText != null, "laserText cannot be null");
            Debug.Assert(levelText != null, "levelText cannot be null");
            
            startButton.onClick.AddListener(OnStartButtonClicked);
            restartButton.onClick.AddListener(OnRestartButtonClicked);
            quitButton.onClick.AddListener(OnQuitButtonClicked);
            nextLevelButton.onClick.AddListener(OnNextLevelClicked);
            
            restartButton.gameObject.SetActive(false);
            quitButton.gameObject.SetActive(false);
            nextLevelButton.gameObject.SetActive(false);
            laserText.gameObject.SetActive(false);
        }

        private void OnStartButtonClicked()
        {
            dialog.gameObject.SetActive(false);
            laserText.gameObject.SetActive(true);
            scoreText.gameObject.SetActive(true);
            GameManager.Instance.StartGame();
        }

        private void OnRestartButtonClicked()
        {
            startButton.gameObject.SetActive(true);
            dialog.gameObject.SetActive(false);
            restartButton.gameObject.SetActive(false);
            quitButton.gameObject.SetActive(false);
            nextLevelButton.gameObject.SetActive(false);
            text.gameObject.SetActive(true);
            scoreText.gameObject.SetActive(false);
            text.text = "Space Shooter";
            
            SoundManager.Instance.Stop();

            GameManager.Instance.Restart();
        }

        private void OnQuitButtonClicked()
        {
            Application.Quit();
            Debug.Log("Game is exiting");
        }

        private void OnNextLevelClicked()
        {
            startButton.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(false);
            quitButton.gameObject.SetActive(false);
            nextLevelButton.gameObject.SetActive(false);
            laserText.gameObject.SetActive(false);
            text.text = "Space Shooter";
            
            SoundManager.Instance.Stop();
            GameManager.Instance.NextLevel();
            
            levelText.text = $"Level : {GameManager.Instance.GetLevel()}";
        }
        
        public void HideScore(bool hide) 
        { 
            scoreText.gameObject.SetActive(!hide); 
        }

        public void EnableNextLevel()
        {
            nextLevelButton.gameObject.SetActive(true);
        }

        public void End()
        {
            startButton.gameObject.SetActive(false);
            dialog.gameObject.SetActive(true);
            restartButton.gameObject.SetActive(true);
            quitButton.gameObject.SetActive(true);
            text.gameObject.SetActive(true);
            scoreText.gameObject.SetActive(false);
            laserText.gameObject.SetActive(false);
            text.text = $"Score : {ScoreManager.Instance.ShowScore()}";
        }

        public bool CheckActive()
        {
            return dialog.gameObject.activeInHierarchy;
        }

        public void SetDialogActive(bool show)
        {
            dialog.gameObject.SetActive(show);
        }
        
        public void SetScoreText(string newText)
        {
            scoreText.text = newText;
        }

        public void SetLaserText(string newText)
        {
            laserText.text = newText;
        }
    }
}