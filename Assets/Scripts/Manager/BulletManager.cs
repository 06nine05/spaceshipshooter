using DefaultNamespace;
using Spaceship;
using UnityEngine;

namespace Manager
{
    public class BulletManager : Singleton<BulletManager>
    {
        [SerializeField] private Bullet smallBullet;
        [SerializeField] private Bullet largeBullet;

        public Bullet GetSmallBullet()
        {
            return smallBullet;
        }

        public Bullet GetLargeBullet()
        {
            return largeBullet;
        }
    }
}