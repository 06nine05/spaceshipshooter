using DefaultNamespace;
using TMPro;
using UnityEngine;

namespace Manager
{
    public class ScoreManager : Singleton<ScoreManager>
    {
        private GameManager gameManager;
        private int score;
        private int scoreBefore;
        
        public void Init(GameManager gameManager) 
        { 
            this.gameManager = gameManager; 
            this.gameManager.OnRestarted += OnRestarted; 
            UIManager.Instance.HideScore(true);
            score = 0;
            scoreBefore = 0;
        }
        
        public void AddScore()
        {
            score++;
        }
        
        private void Awake() 
        {
            UIManager.Instance.HideScore(true);
        }
        
        private void OnRestarted() 
        { 
            gameManager.OnRestarted -= OnRestarted; 
            UIManager.Instance.HideScore(true);
        }

        public void ResetLevelScore()
        {
            score = scoreBefore;
        }

        public void NewScore()
        {
            scoreBefore = score;
        }
        
        public int ShowScore()
        {
            return score;
        }
    }
}