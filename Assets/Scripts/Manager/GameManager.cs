using System;
using DefaultNamespace;
using Spaceship;
using UnityEngine;

namespace Manager
{
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private EnemySpaceship enemySpaceship;

        public event Action OnRestarted;
        private int enemyCount;
        private int enemyNumber;
        private PlayerSpaceship spaceship;
        private EnemySpaceship enemy;
        private int level;

        private void Awake()
        {
            Debug.Assert(playerSpaceship != null, "playerSpaceship cannot be null");
            Debug.Assert(enemySpaceship != null, "enemySpaceship cannot be null");
            
            ScoreManager.Instance.Init(this);

            level = 1;
            enemyNumber = 1 + 2 * (Instance.level - 1);
            enemyCount = enemyNumber;
        }

        public void StartGame()
        {
            SpawnPlayerSpaceship();
            SpawnEnemySpaceship();
            SoundManager.Instance.PlayBGM();
        }

        public void NextLevel()
        {
            level++;
            enemyNumber = enemyNumber = 1 + 2 * (Instance.level - 1);
            enemyCount = enemyNumber;
        }

        public int GetLevel()
        {
            return level;
        }
        
        private void SpawnPlayerSpaceship()
        {
            spaceship = Instantiate(playerSpaceship);
            spaceship.Init(100, 50f);
            spaceship.OnExploded += OnPlayerSpaceshipExploded;
        }

        private void OnPlayerSpaceshipExploded()
        {
            Lose();
            Restart();
        }

        private void SpawnEnemySpaceship()
        {
            for (var i = 0; i < enemyNumber; i++)
            {
                var xPos = UnityEngine.Random.Range(-55, 55);

                enemy = Instantiate(enemySpaceship, new Vector3(xPos, 30 , 0), Quaternion.identity);
                enemy.Init(100, 30f);
                enemy.OnExploded += OnEnemySpaceshipExploded;
            }
        }

        private void OnEnemySpaceshipExploded()
        {
            ScoreManager.Instance.AddScore();
            enemyCount--;
            
            UIManager.Instance.SetScoreText($"Score : {ScoreManager.Instance.ShowScore()}");
        }

        public void Restart()
        {
            UIManager.Instance.SetDialogActive(true);
            UIManager.Instance.SetLaserText("Laser : 5");
            OnRestarted?.Invoke();
            
            enemyNumber = enemyNumber = 1 + 2 * (Instance.level - 1);
            enemyCount = enemyNumber;
        }

        private void Update()
        {
            if (enemyCount == 0 && UIManager.Instance.CheckActive() == false)
            {
                Win();
            }
            
            Debug.Log(enemyNumber);
        }

        private void Win()
        {
            Destroy(GameObject.FindWithTag("Player"));
            UIManager.Instance.End();
            ScoreManager.Instance.NewScore();
            UIManager.Instance.EnableNextLevel();
            
        }
        
        private void Lose()
        {
            var enemies = GameObject.FindGameObjectsWithTag("Enemy");

            for (var i = 0; i < enemies.Length; i++)
            {
                Destroy(enemies[i]);
            }
            
            UIManager.Instance.End();
            ScoreManager.Instance.ResetLevelScore();
            UIManager.Instance.SetScoreText($"Score : {ScoreManager.Instance.ShowScore()}");
        }
    }
}