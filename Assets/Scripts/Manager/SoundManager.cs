﻿using System;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;
        [SerializeField] private AudioSource bgmSource;
        [SerializeField] private AudioSource audioSource;
        
        public static SoundManager Instance { get; private set; }
        
        [Serializable]
        public struct SoundClip
        {
            public Sound Sound;
            public AudioClip AudioClip;
        }
        
        public enum Sound
        {
            BGM,
            BulletSmall,
            PlayerExplode,
            BulletLarge,
            EnemyExplode,
            Laser,
        }
        
        /// <summary>
        /// Play sound
        /// </summary>
        /// <param name="audioSource"></param>
        /// <param name="sound"></param>
        public void Play(AudioSource audioSource, Sound sound)
        {
            Debug.Assert(audioSource != null, "audioSource cannot be null");

            audioSource.clip = GetAudioClip(sound);
            audioSource.Play();
        }

        public void PlayBGM()
        {
            Play(bgmSource, Sound.BGM);
        }

        public void PlayEnemyDeath()
        {
            Play(audioSource, Sound.EnemyExplode);
        }

        public void PlayPlayerDeath()
        {
            Play(audioSource, Sound.PlayerExplode);
        }

        public void Stop()
        {
            bgmSource.Stop();
        }

        private AudioClip GetAudioClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.Sound == sound)
                {
                    return soundClip.AudioClip;
                }
            }
            
            Debug.Assert(false,$"Cannot find sound {sound}");
            return null;
        }

        private void Awake()
        {
            Debug.Assert(audioSource != null, "audioSource cannot be null");
            Debug.Assert(soundClips != null, "soundClips cannot be null");

            if (Instance == null)
            {
                Instance = this;
            }
            
            DontDestroyOnLoad(this);
        }
    }
}
