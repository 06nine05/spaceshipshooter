using System;
using DefaultNamespace;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : BaseSpaceship, IDamagable
    {
        [SerializeField] private Bullet laser;
        [SerializeField] private Transform laserPosition;
        
        public event Action OnExploded;
        private int laserNo;

        private void Awake()
        {
            defaultBullet = BulletManager.Instance.GetSmallBullet();
            laserNo = 5;
            
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(audioSource != null,"audioSource cannot be null");
            Debug.Assert(laser != null, "laser cannot be null");
            Debug.Assert(laserPosition != null, "laserPosition cannot be null");
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init();

            if (defaultBullet.type == Bullet.BulletType.BulletSmall)
            {
                SoundManager.Instance.Play(audioSource, SoundManager.Sound.BulletSmall);
            }

            else if (defaultBullet.type == Bullet.BulletType.BulletLarge)
            {
                SoundManager.Instance.Play(audioSource, SoundManager.Sound.BulletLarge);
            }
            
        }

        public void ChangeBullet()
        {
            if (defaultBullet.type == Bullet.BulletType.BulletSmall)
            {
                defaultBullet = BulletManager.Instance.GetLargeBullet();
            }

            else if (defaultBullet.type == Bullet.BulletType.BulletLarge)
            {
                defaultBullet = BulletManager.Instance.GetSmallBullet();
            }
        }

        public void LaserFire()
        {
            if (laserNo > 0)
            {
                var laser = Instantiate(this.laser, laserPosition.position, Quaternion.identity);
                laser.Init();
            
                SoundManager.Instance.Play(audioSource, SoundManager.Sound.Laser);
                laserNo--;
                
                UIManager.Instance.SetLaserText($"Laser : {laserNo}");
            }

            else
            {
                Debug.Log("No laser left");
            }
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }

            Explode();
        }

        public void Explode()
        { 
            SoundManager.Instance.PlayPlayerDeath();
            Debug.Assert(Hp <= 0, "Hp is more than zero ");
            Debug.Log("You are dead bro!");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            Debug.Log("HIT");
        }
    }
}