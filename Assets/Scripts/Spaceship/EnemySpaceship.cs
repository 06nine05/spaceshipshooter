using System;
using DefaultNamespace;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;

        private void Awake()
        {
            defaultBullet = BulletManager.Instance.GetLargeBullet();
            
            Debug.Assert(defaultBullet != null, "defaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            Debug.Assert(audioSource != null,"audioSource cannot be null");
        }
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }

            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.PlayEnemyDeath();
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.MoveDown();
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.BulletLarge);
        }
    }
}