﻿using UnityEngine;

namespace Spaceship
{
    public abstract class BaseSpaceship : MonoBehaviour
    {
        [SerializeField] protected Transform gunPosition;
        [SerializeField] protected AudioSource audioSource;
        public Bullet defaultBullet;
        
        public int Hp { get; protected set; }
        public float Speed { get; private set; }
        public Bullet Bullet { get; private set; }

        protected void Init(int hp, float speed, Bullet bullet)
        {
            Hp = hp;
            Speed = speed;
            Bullet = bullet;
        }

        public void Remove()
        {
            Destroy(gameObject);
        }

        public abstract void Fire();
    }
}